# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import docko_test_repo
version = docko_test_repo.__version__

setup(
    name='docko_test_repo',
    version=version,
    author='',
    author_email='thordurguhe@gmail.com',
    packages=[
        'docko_test_repo',
    ],
    include_package_data=True,
    install_requires=[
        'Django>=1.6.5',
    ],
    zip_safe=False,
    scripts=['docko_test_repo/manage.py'],
)
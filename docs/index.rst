.. docko_test_repo documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to docko_test_repo's documentation!
====================================================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   deploy
   tests



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`